/**
 * Demo file for the multi-parameter feature of Parameters.h
 */
#include "Parameters.h"

Parameters p;

#define PARAMETERS_CLASS SimuParameters
#define PARAMETERS_DEF "SimuParameters-def.h"
#include "Parameters.h"

SimuParameters simu;


using namespace std;

int main(int argc, char ** argv)
{
	// Same as default
	p.allowUnknown(false);
	p.paramEqValue(false);

	MultiParameters mp;
	mp.add(p);
	mp.add(simu);
	mp.parse(argc, argv);
	mp.list(cout);

	cout << endl;
	for (int i = 1 ; i < argc ; ++i)
		cout << "Remaining parameter " << i << ": " << argv[i] << endl;

	return 0;
}
