/**
 * Demo file for Parameters.h
 */

#include "Parameters.h"

// The parameter class is a global variable
Parameters p;

using namespace std;

int main(int argc, char ** argv)
{
	// Same as default
	p.allowUnknown(false);
	p.paramEqValue(false);

	cout << "List of parameters at startup (with default values):" << endl;
	p.list(cout);
	cout << endl;

	p.read("params.conf");
	cout << "List of parameters after reading params.conf:" << endl;
	p.list(cout);
	cout << endl;

	p.parse(argc, argv);
	cout << "\nList of parameters after parsing command line:" << endl;
	p.list(cout);
	cout << endl;

	// Access to parameters is easy
	cout << "ngnus is " << p.ngnus <<endl;
	// Parameters can be changed too
	p.simname = "secret_life_of_pets";
	cout << endl;

	cout << "\nAll parameters remain when allow_unknown==false" << endl;
	for (int i = 1 ; i < argc ; ++i)
		cout << "Remaining parameter " << i << ": " << argv[i] << endl;

	return 0;
}
