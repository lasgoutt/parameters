/*
  This file is input multiple times by Parameters.h in order to define
  the parameters accepted by the program. See description of the
  syntax in README file.

  To add a new parameter, it is enough to change this file.
*/

HELP("Simulation settings")
PARAM(int, year, -2349, "Starting year")
PARAM(int, days, 220, "Duration in days")

