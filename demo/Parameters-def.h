/*
  This file is input multiple times by Parameters.h in order to define
  the parameters accepted by the example program `testparams.cpp`.

  See description of the syntax in README.md file.

  To add a new parameter, it is enough to change this file.
*/

#ifdef IN_PREAMBLE
enum Animal {GNU, GNAT};
#endif

PARAM(std::string, simname, "noas_ark_tng", "Name of the simulation")

PARAM_ENUM(Animal, animal, GNU, "animal of interest")
VALUE_ENUM(animal, GNU, "only gnus count")
VALUE_ENUM(animal, GNAT, "long live to gnats!")

HELP("Gnus parameters")
PARAM(int, ngnus, 100, "Number of gnus in savannah")
PARAM(int, gnu_tails, 1, "Number of tails per gnu")
HELP("Gnats parameters")
PARAM(int, ngnats, 100, "Number of gnats in the room")
PARAM(int, gnat_tails, 0, "Number of tails per gnat")

