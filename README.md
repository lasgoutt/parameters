Parameters handling in C++ with Parameters.h
============================================

This header file defines the class Parameters that provides access to
software parameters defined through the C preprocessor, using the not
so well-known [X-Macros](https://en.wikipedia.org/wiki/X_Macro)
technique. All the parameters are accessible as normal members of the
class.

Why use it?
- because when writing small programs, parameters handling takes too much time.
- because relying on a real library is cumbersome.
- because I like writing weird macros :)

The parameters can have default values and be associated to a help string.
Their list can be given by the program. There is also support for setting
parameters on the command line or a configuration file.

Assume that `param1` is an integer, that `param2` is a string and `param3` is an
enumeration that can take values `BAR` and `BAZ`. There are two different
syntaxes available for setting parameters from the command-line.

- `param=value` : similar to syntax of the make program

   Example:
   ```
   myprog param1=2 param2=foo param3=bar
   ```

- `-param value` : similar to common unix options

  Example:
  ```
  myprog -param1 2 -param2 foo -bar
  ```

  In this case it can be seen that for `param3`, only the value of
  the enum is used. It is supposed to be unique.

The parameter names and enumeration values are matched in a
case-insensitive manner. It is possible to use only the few first
letter of parameters names, as long as they are not ambiguous.

Only the `Parameters.h` file is needed from this archive. The other
files contained are only required for the examples. You should
provide your own `Parameters-def.h` file(s) to configure the parameters.

The example program `demo/params.cpp`, along with the
`demo/Parameters-def.h` file, show how easy it is to use this class.

Defining parameters in `Parameters-def.h`
-----------------------------------------

The definition of parameters is done through a series of macros. The
most important is the first one, `PARAM`.

### `PARAM(type, name, default, description)`

This declares a valued parameter that can be set from command line.
Arguments are:

- `type` (`bool`, `string`, `int`,...): the type of the underlying variable.
  The type should be readable and writable by a stream using the `<<` and `>>`
  operators.

- `name`: the name of the parameter (used both in config files and as
  name in the code).

- `default`: the default value of the parameter.

- `description`: a help text

Example:
```c++
PARAM(int, ngnus, 100, "Number of gnus in savannah")
```

### `IN_PREAMBLE`

This macro can be used to guard types declaration or anything that
should happen before the `Parameters` class declaration.

Example:
```c++
#ifdef IN_PREAMBLE
enum Animal {GNU, GNAT};
#endif
```

### `PARAM_ENUM(type, name, default, description)`

 This declares an enum parameter, that will be set using switches on
the command line. Arguments are:

- `type`: an enum type.

- `name`: the name of the parameter (used both in config files and as
  name in the code).

- `default`: the default value of the parameter.

- `description`: a help text

Example:
```c++
#ifdef IN_PREAMBLE
enum Animal {GNU, GNAT};
#endif
PARAM_ENUM(Animal, animal, GNU, "animal of interest")
```

### `VALUE_ENUM(name, value, description)`

This declares a switch that can set an enum from the command-line.
The enum is declared using the `PARAM_ENUM` macro. Arguments are:

- `name`: the name of the parameter (used both in config files and as
  name in the code).

- `value`: the particular value of the parameter.

- `description`: a help text for this particular value

Example: here we define switches `-gnu` and `-gnats` that set value of
parameter `animal`
```c++
#ifdef IN_PREAMBLE
enum Animal {GNU, GNAT};
#endif
PARAM_ENUM(Animal, animal, GNU, "animal of interest")
VALUE_ENUM(animal, GNU, "only gnus count")
VALUE_ENUM(animal, GNAT, "long live to gnats!")
```

### `HELP(help_string)`

This adds a title string that can be to separate settings in
different categories. Note that this only makes sense for `PARAM`
types of parameters. Argument is:

- `help_string`: the help string

  Example:
  ```c++
  HELP("Gnus parameters")
  PARAM(int, ngnus, 100, "Number of gnus in savannah")
  PARAM(int, gnu_tails, 1, "Number of tails per gnu")
  HELP("Gnats parameters")
  PARAM(int, ngnats, 100, "Number of gnats in the room")
  PARAM(int, gnat_tails, 0, "Number of tails per gnat")
  ```

Using the Parameters API
------------------------

Declaring parameters is as easy as
```c++
#include "Parameters.h"
Parameters p;
```

In this case, the parameters will be defined in `Parameters-def.h`.
If the file needs to have another name, it suffices to do
```c++
#define PARAMETERS_DEF "Parameters-special.h"
#include "Parameters.h"
Parameters p;
```

### `void parse(int argc, char ** argv)`

Parse the known parameters of the command line and set the relevant
values. You probably to use at least this function, or `read()`,
or both.

`argc` and `argv` are as provided to the `main()` function.

Parameter names are case-insensitive and can be shortened as long as
they are not ambiguous.

### `bool read(std::string const & fname)`

Read file `fname`, which contains a list of parameters to set.
The format is
```
# comments start with hash sign
param value
param value # comments can be anywhere
```

Parameter names are case-insensitive.

Returns false when the file could not be read.

### `void commentPrefix(std::string const & str)`

Change the string that indicates the start of a comment.

Default is `#`.

The getter `commentPrefix()` returns the current comment prefix.

### `void paramEqValue(bool b)`

If true, the syntax for setting a parameter on command line is
`param=value`. Otherwise, the syntax is `-param value`.

Default is `false` (`-param value`).

The getter `paramEqValue()` returns the current setting.


### `void allowUnknown(bool b)`

If `b=true`, the command line arguments not recognized by the parser
have to be handled by the application. Otherwise, they trigger an
error. They will be kept in argc/argv.

Conversely, when `b` is `false`, the parser errors
out when an unknown parameter is encountered. Additionally, it
understands a `-h` (or `-help`) option that displays a help
message and exits.

Default is `false` (error on unknown command line parameter).

The getter `allowUnknown()` returns the current setting.


Advanced API
-----------

The following methods are only useful for debugging or creating a custom
a help command (when `allowUnknown()` is true).

### `void list(std::ostream & os)`

Output to the stream `os` a list of the parameters along with
their current values and a description.

### `void help_switch(std::ostream & os)`

Output to the stream `os` a list of available value-less options (only
useful with `param=value` syntax), along with the default values and a
description.

### `void help_values(std::ostream & os, bool param_eq_value)`

Output to the stream `os` a list of available parameters (along with
the enums with `param=value` syntax), along with the default values and a
description.

## Using several sets of parameters: the `Multiparameters` class

If more than one set of parameters is needed, one can do
```c++
// first set of parameters
#include "Parameters.h"
Parameters p;

#define PARAMETERS_CLASS MoreParameters
#define PARAMETERS_DEF "MoreParameters-def.h"
#include "Parameters.h"
MoreParameters morep;
```

We now have two sets of parameters, that can for example be used in
different files. If we need to parse them, we need a unifying view,
provided by the `MultiParameters` class.

This class is like any `Parameters` class, except that it has an
additional method.

### `void add(ParametersBase & p)`

This method adds the parameters set `p` to the list of parameters
unified by `MultiParameters`.

Example: in the above case, one could use in `main()`
```c++
MultiParameters mp;
mp.add(p);
mp.add(morep);
mp.parse(argc, argv);
```

This will parse all the parameters sets as needed.
